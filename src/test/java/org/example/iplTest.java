package org.example;
import org.junit.jupiter.api.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.*;
import java.util.Scanner;
//@TestInstance(TestInstance.Lifecycle.PER_CLASS)

public class iplTest {

    @BeforeAll
    static void beforeClass(){
        System.out.println("Running..........");
    };

    @BeforeEach
     void beforeEach(){
        System.out.println("Running before each");
    };

    @AfterEach

    void afterEach() {
        System.out.println("After Each");
    }

    @Test
    @DisplayName("IPL checking assertEquals in Number of matches played per year of all the years in IPL.")
    public void testMatchesPerYear(){

        Map<String, Integer> matchesPerYear = matchPerYear.iplMatchCounter();

        Map<String, Integer> expectedMatchesPerYear = new HashMap<>();

        expectedMatchesPerYear.put("2009", 57);
        expectedMatchesPerYear.put("2008", 58);
        expectedMatchesPerYear.put("2017", 59);
        expectedMatchesPerYear.put("2016", 60);
        expectedMatchesPerYear.put("2015", 59);
        expectedMatchesPerYear.put("2014", 60);
        expectedMatchesPerYear.put("2013", 76);
        expectedMatchesPerYear.put("2012", 74);
        expectedMatchesPerYear.put("season", 1);
        expectedMatchesPerYear.put("2011", 73);
        expectedMatchesPerYear.put("2010", 60 );

        Assertions.assertEquals(expectedMatchesPerYear, matchesPerYear);
    }

    @Test
    @DisplayName("IPL cehcking NOT NULL Number of matches won of all teams over all the years of IPL.")
     public void matchesWonPerTeamPerYear(){
        Map<String, Map<String, Integer>> matchesWonPerTeamPerYear = new HashMap<>();

        matchesWonPerTeamPerYear = org.example.matchesWonPerTeamPerYear.count();

        Map<String, Integer> rajasthanRoyal = matchesWonPerTeamPerYear.get("Rajasthan Royals");

        Assertions.assertNotNull(rajasthanRoyal);
    };

    @AfterAll

    static void afterAll(){
        System.out.println("completed");
    }



}
