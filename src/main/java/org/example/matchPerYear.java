package org.example;
import java.util.*;
import java.io.File;
import java.io.FileNotFoundException;

public class matchPerYear {
    public static Map<String, Integer> iplMatchCounter() {

        String csvFilePath = "./data/matches.csv";

        Map<String, Integer> matchesPerYear = new HashMap<>();

        try (Scanner scanner = new Scanner(new File(csvFilePath))) {

            while (scanner.hasNextLine()) {

                String line = scanner.nextLine();

                String[] fields = line.split(",");

                String season = fields[1].trim();
                matchesPerYear.put(season, matchesPerYear.getOrDefault(season, 0) + 1);
            }

        } catch (FileNotFoundException e) {
            System.out.println("file not found");
        }

        return matchesPerYear;
    };

    public static void main(String[] args) {
        Map<String, Integer> matchesPerYear = new HashMap<>();

        matchesPerYear = iplMatchCounter();
        for (Map.Entry<String, Integer> entry : matchesPerYear.entrySet()) {
            System.out.println("Season: " + entry.getKey() + ", Matches: " + entry.getValue());
        }
    }
}
