package org.example;
import java.util.*;
import java.io.File;
import java.io.FileNotFoundException;
public class matchesWonPerTeamPerYear {
    public static  Map<String, Map<String, Integer>> count(){
        String csvFilePath = "./data/matches.csv";

        Map<String, Map<String, Integer>> matchesWonPerTeamPerYear = new HashMap<>();

        try (Scanner scanner = new Scanner(new File(csvFilePath))) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] fields = line.split(",");

                String winnerTeam = fields[10].trim();
                String season = fields[1].trim();

                if (!matchesWonPerTeamPerYear.containsKey(winnerTeam)) {
                    matchesWonPerTeamPerYear.put(winnerTeam, new HashMap<>());
                }

                Map<String, Integer> innerMap = matchesWonPerTeamPerYear.get(winnerTeam);

                innerMap.put(season, innerMap.getOrDefault(season, 0) + 1);
            }

        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }

        return matchesWonPerTeamPerYear;
    };
}
